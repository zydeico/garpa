-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-08-2018 a las 07:09:38
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `garpafacil`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configs`
--

CREATE TABLE `configs` (
  `ruta_root` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `ruta_file` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `subida` int(255) NOT NULL,
  `foto` varchar(180) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `configs`
--

INSERT INTO `configs` (`ruta_root`, `ruta_file`, `subida`, `foto`) VALUES
('/garpafacil/file/', 'file/', 1100000, 'https://i.imgur.com/l14twX8.png');

-- --------------------------------------------------------
CREATE TABLE `recuperar` (
  `id` int(255) NOT NULL,
  `correo` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `iden` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE `recuperar`
  ADD PRIMARY KEY (`id`);
ALTER TABLE `recuperar`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Estructura de tabla para la tabla `deposito`
--

CREATE TABLE `deposito` (
  `id` int(255) NOT NULL,
  `iden` int(180) NOT NULL,
  `correo` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `deposito` int(255) NOT NULL,
  `extra` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envio`
--

CREATE TABLE `envio` (
  `id` int(255) NOT NULL,
  `iden` int(255) NOT NULL,
  `correo` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `referencia` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `servicio` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `tarifa` int(255) NOT NULL,
  `detalle` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `archivo` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `progreso` int(255) NOT NULL,
  `tipo` int(1) NOT NULL,
  `estado` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificacion`
--

CREATE TABLE `notificacion` (
  `id` int(255) NOT NULL,
  `iden` int(180) NOT NULL,
  `correo` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `mensaje` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagar`
--

CREATE TABLE `pagar` (
  `id` int(255) NOT NULL,
  `iden` int(255) NOT NULL,
  `correo` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `servicio` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `deposito` int(255) NOT NULL,
  `tarifa` int(255) NOT NULL,
  `detalles` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclamos`
--

CREATE TABLE `reclamos` (
  `id` int(255) NOT NULL,
  `correo` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `numero` int(200) NOT NULL,
  `detalles` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id` int(255) NOT NULL,
  `nombre` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `icono` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `enlace` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`id`, `nombre`, `descripcion`, `icono`, `enlace`, `estado`) VALUES
(1, 'CLARO', 'Empresa de telefonía movil de Argentina.', 'celular', 'http://www.movistar.com.ar/mi-movistar', 1),
(3, 'Telecom', 'Empresa de telefonía e internet de Argentina.', 'comunicacion', 'https://sesion.telecom.com.ar/', 1),
(4, 'EPEC', 'Empresa provincial de energía electrica.', 'lampara', 'https://www.epec.com.ar/hogares-login.html?m=21=21', 1),
(5, 'DirecciÃ³n General de Rentas', 'Administración y recaudación de las Acreencias Tributarias y No Tributarias de la Provincia de Córdoba ', 'desarrollo', 'https://www.rentascordoba.gob.ar/mirentas/rentas.html?page=pagosdeimpuestos', 1),
(6, 'Municipalidad de CÃ³rdoba', 'Portal Tributario del municipio de Córdoba', 'desarrollo', 'https://servicios2.cordoba.gov.ar/Tributario/Default.aspx#no-back-button', 1),
(7, 'Aguas Cordobesas', 'Empresa de servicio de agua potable', 'agua', 'https://www.aguascordobesas.com.ar/espacioClientes/action/factura-digital', 1),
(8, 'Movistar', 'Empresa de telefonía móvil de Argentina', 'comunicacion', 'http://www.movistar.com.ar/mi-factura', 1),
(9, 'Policia caminera', 'Consulta de multas', 'auto-licencia-ok', 'https://www.rentascordoba.gob.ar/mirentas/rentas.html?page=caminera', 1),
(10, 'Camino de las sierras', 'Consultas de servicios de RAC', 'auto-licencia-ok', 'https://www.caminosdelassierras.com.ar/seccion/ingresar', 1),
(11, 'Tuenti', 'Empresa de telefonía móvil de Argentina', 'comunicacion', 'https://www.tuenti.com.ar/recarga/publica/', 1),
(12, 'AFIP', 'Administración Federal de Ingresos Públicos', 'pago', 'https://auth.afip.gob.ar/contribuyente_/login.xhtml', 1),
(13, 'Ecogas', 'Empresa de gas natural de Argentina', 'gas', 'https://www.ecogas.com.ar/appweb/leo/sec/ui-gestion-en-linea-reimp.php', 1),
(14, 'CablevisiÃ³n y Fibertel', 'Empresa de internet y cable de Argentina', 'comunicacion', 'https://bit.ly/2Me29IA', 1),
(15, 'Supercanal', 'Empresa de cableoperador de Argentina', 'comunicacion', 'https://sucursalvirtual.supercanal-arlink.com.ar/', 1),
(16, 'Personal', 'Empresa de telefonía móvil de Argentina', 'comunicacion', 'https://miperfil.personal.com.ar/login/', 1),
(17, 'Nextel', 'Empresa de telefonía móvil de Argentina', 'comunicacion', 'https://webapp2.nextel.com.ar/autogestion/login.seam', 1),
(18, 'Hostinger', 'Empresa de alojamiento web', 'inclusion-digital', 'https://www.hostinger.com.ar/cpanel-login', 1),
(19, 'Donweb', 'Empresa de alojamiento web', 'inclusion-digital', 'https://donweb.com/es-ar/ingresar', 1),
(20, 'Multas Municipalidad de CÃ³rdoba', 'Tribunal de faltas de la Ciudad de Córdoba', 'auto-licencia-ok', 'https://servicios2.cordoba.gov.ar/Tributario/Tributo.aspx?t=Fa2L3i#no-back-button', 1),
(21, 'Taxis y Remises', 'Aranceles de taxis y remises de la Ciudad de Córdoba', 'auto-licencia-ok', 'https://servicios2.cordoba.gov.ar/Tributario/Tributo.aspx?t=JJkp87#no-back-button', 1),
(22, 'La caja seguros', 'Empresa aseguradora', 'seguridad', 'https://bit.ly/1Kr8KED', 1),
(23, 'Sancor Seguros', 'Empresa aseguradora', 'seguridad', 'https://bit.ly/2OZQfRi', 1),
(24, 'Lojack seguros', 'Empresa aseguradora', 'seguridad', 'http://misfacturas.lojacklatam.com/', 1),
(25, 'Orbis seguros', 'Empresa aseguradora', 'seguridad', 'https://asegurados.orbiseguros.com.ar/', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servidor`
--

CREATE TABLE `servidor` (
  `cuerpo` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `titulo` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `de` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `servidor` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `puerto` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `servidor`
--

INSERT INTO `servidor` (`cuerpo`, `titulo`, `de`, `servidor`, `usuario`, `contrasena`, `puerto`) VALUES
('GarpaFacil', 'Prueba GarpaFacil', 'alanmoyano018@gmail.com', 'in-v3.mailjet.com', 'beb6391cae7140a434a5e1c9b9c05f8e', '210a9c27b942d432cee3fe4ecb247d6d', 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(255) NOT NULL,
  `nombre` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(180) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `numero` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `deposito` int(255) NOT NULL,
  `foto` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `rango` int(1) NOT NULL,
  `conectado` varchar(1) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`ruta_root`);

--
-- Indices de la tabla `deposito`
--
ALTER TABLE `deposito`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `envio`
--
ALTER TABLE `envio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notificacion`
--
ALTER TABLE `notificacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pagar`
--
ALTER TABLE `pagar`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servidor`
--
ALTER TABLE `servidor`
  ADD PRIMARY KEY (`cuerpo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `deposito`
--
ALTER TABLE `deposito`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `envio`
--
ALTER TABLE `envio`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `notificacion`
--
ALTER TABLE `notificacion`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pagar`
--
ALTER TABLE `pagar`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
