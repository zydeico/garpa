<?php include("conexion.php");  ?>
<?php

$consulta_lago = $conexion->query("SELECT * FROM argentina WHERE estado='1'");

?>

<!-- Argentina -->
<div id="argentina" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title"><i class="flag-icon flag-icon-ar"></i> Argentina</h4>
      </div>
      <div class="modal-body">

      <div class="form-group">
        <input class="form-control" id="yobusco" type="text" placeholder="Buscar">
      </div>

      <div class="row">
        <?php if(mysqli_num_rows($consulta_lago)>0) {?>
          <?php while($info = mysqli_fetch_array($consulta_lago)) {?>
            <div class="buscar col-md-4">
              <a class="panel panel-default panel-icon panel-secondary" href="<?php echo $info['url']; ?>">
                <div style="width: 100% ;background-image:url('<?php echo $info['imagen']; ?>'); background-size:100%; background-repeat: no-repeat;" class="panel-heading"></div>
                  <div class="panel-body">
                  <h3><b><?php echo $info['provincia']; ?></b></h3>
                </div>
              </a>
            </div>
          <?php } ?>
        <?php } else { ?>
          <div class="col-md-12">
            <h3 class="bg-danger text-white text-center">No se encuentran lagos</h3>
          </div>
        <?php } ?>

      </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
