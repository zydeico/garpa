<?php error_reporting(E_ALL ^ E_NOTICE);
 ?>
<meta charset="utf-8">
<meta content="width=device-width,initial-scale=1" name=viewport>
<link rel="stylesheet" type="text/css" href="../../vendor/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../vendor/css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="../../vendor/css/estilos.css">
<link rel="stylesheet" href="https://argob.github.io/iconos/dist/css/icono-arg.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.css">

  <meta name="theme-color" content="#0072b8">
  <meta name="msapplication-navbutton-color" content="#0072b8">
  <meta name="apple-mobile-web-app-status-bar-style" content="#0072b8">
