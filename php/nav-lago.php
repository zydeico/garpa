<nav class="navbar navbar-default fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><img src="https://i.imgur.com/uWd7vJz.png" height="50" alt=""><b>  &nbsp;&nbsp;Earth Is Water</b></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class=""><a href="../../index.php"><b><i class="fa fa-home"></i> Regresar a inicio</b></a></li>


        <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>Satelite Landsat 7</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> 12 de junio 12:39</p>
                                        <p>Faltan 5 días para el paso del Landsat 7 en este lago, preparen sus medidores!</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview bg-danger">
                            <a href="#">
                                <div class="media">
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>Medición promedio</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> 12 de junio 12:39</p>
                                        <p>Asociación Cordobesa del remo ha marcado eutrófico en su zona</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>Satelite Landsat 7</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> 12 de junio 12:39</p>
                                        <p>Faltan 5 días para el paso del Landsat 7 en este lago, preparen sus medidores!</p>
                                    </div>
                                </div>
                            </a>
                        </li>

                    </ul>
                </li>


      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
