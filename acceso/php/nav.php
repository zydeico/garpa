<!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-dark navbar-light border-bottom fixed-top">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i> <?php if(mysqli_num_rows($mcon)>0) { ?>
          <span class="badge badge-info right"><?php echo mysqli_num_rows($mcon); ?></span>
        <?php } ?></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Gestione sus facturas y pagelas en el momento.</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <li class="nav-item bg-danger">
        <a class="nav-link"  href="../salir.php"><i class="fa fa-sign-out"></i> Cerrar sesión</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link bg-success">
      <img src="https://i.imgur.com/oOvdOr7.png" alt="Garpa Facil logo" class="brand-image ">
      <span class="brand-text font-weight-light">Garpa Fácil</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img class="img-circle" src="<?php if(strpos($info['foto'], 'https://') !== false) { echo $lconfig['foto']; } else { echo '../../'; echo $lconfig['ruta_file']; echo $info['foto']; } ?>">
        </div>
        <div class="info">
          <a href="ajustes.php" class="d-block"><b><?php echo $info['nombre']; echo ' '; echo $info['apellido']; ?></b></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">


          <li class="nav-item active">
            <a href="inicio.php" class="nav-link <?php echo $active_inicio; ?>">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>Inicio</p>
            </a>
          </li>
          <?php if($info['rango'] == 0) { ?>
            <li class="nav-item active">
              <a href="pendientes-envios.php" class="nav-link <?php echo $active_enviados; ?>">
                <i class="nav-icon fa fa-envelope-o"></i>
                <p>Pendiente de envio</p>
              </a>
            </li>
            <li class="nav-item active">
              <a href="depositos-pendientes.php" class="nav-link <?php echo $active_deposito; ?>">
                <i class="nav-icon fa fa-usd"></i>
                <p>Pendiente de depositos</p>
              </a>
            </li>
            <li class="nav-item active">
              <a href="notificaciones.php" class="nav-link <?php echo $active_noti_envio; ?>">
                <i class="nav-icon fa fa-bell-o"></i>
                <p>Notificaciónes</p>
                <?php if(mysqli_num_rows($mcon)>0) { ?>
                  <span class="badge badge-info right"><?php echo mysqli_num_rows($mcon); ?></span>
                <?php } ?>
              </a>
            </li>
          <?php } ?>
          <li class="nav-item active">
            <a href="ajustes.php" class="nav-link <?php echo $active_ajustes; ?>">
              <i class="nav-icon fa fa-cog"></i>
              <p>Ajustes</p>
            </a>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
