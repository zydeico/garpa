<!-- === DATOS DE LA PAGINA === -->
<?php
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 0) {
    header("Location: ../index.php");
  }

  $ucon2 = $conexion->query("SELECT * FROM usuarios WHERE rango='0'");

  $titulo = "Usuarios | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="icono-arg-comunidad"></i> Usuarios</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="card card-primary card-outline">
          <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                  <tr class="bg-dark text-white">
                    <th>Correo Electronico</th>
                    <th>Nombre y apellido</th>
                    <th>Dinero depositado</th>
                    <th>Editar</th>
                  </tr>
                  <?php if(mysqli_num_rows($ucon2)>0) { ?>
                    <?php while($info2 = mysqli_fetch_array($ucon2)) { ?>
                      <tr>
                        <td><?php echo $info2['correo']; ?></td>
                        <td><?php echo $info2['nombre']; echo ' '; echo $info2['apellido']; ?></td>
                        <td>$<?php echo $info2['deposito']; ?></td>
                        <td>
                          <a href="editar-usuario.php?miembro=<?php echo $info2['correo']; ?>" class="btn btn-primary" title="Editar miembro <?php echo $info2['nombre']; echo ' '; echo $info2['apellido']; ?>"><i class="icono-arg-escritura"></i></a>
                          <a href="../acciones.php?miembro=<?php echo $info2['correo']; ?>" class="btn btn-danger"><i class="icono-arg-reciclaje-basura"></i></a>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } else { ?>
                    <h2 class="text-center mb-3">No hay usuarios</h2>
                  <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>
  <?php include('../php/footer.php'); ?>

</html>
