<!-- === DATOS DE LA PAGINA === -->
<?php
  ob_start();
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 1) {
    header("Location: inicio-admin.php");
  }

  $titulo = "Depositar | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="icono-arg-pago"></i> Depositar dinero</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid mb-5">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <h4>Cualquier depósito tendrá 4,45% + IVA descontado</h4>
              <p>Recuerde que el correo que le debe proporcionar a Mercado Pago debe ser igual al de su cuenta, si no su dinero será devuelto, recuerde guardar cualquier comprobante generado.</p>
              <small class="text-muted">Servicio brindado por Mercado Pago</small>
            </div>
          </div>

          <div class="card card-primary card-outline">
            <div class="card-body">
              <form method="post">
                <div class="form-group">
                  <label for="">Depositar</label>
                  <select class="form-control" name="depositar">
                    <option value="10">10$</option>
                    <option value="20">20$</option>
                    <option value="30">30$</option>
                    <option value="40">40$</option>
                    <option value="50">50$</option>
                    <option value="60">60$</option>
                    <option value="70">70$</option>
                    <option value="80">80$</option>
                    <option value="90">90$</option>
                    <option value="100">100$</option>
                  </select>
                </div>

                <button type="submit" class="mt-3 mb-3 btn btn-block btn-primary" name="enviar">Depositar</button>
                <?php
                  if(isset($_POST['enviar'])) {
                    $usuario = $info['correo'];
                    $deposito = $_POST['depositar'];

                    if($deposito == 10) {
                      $cantidad = "http://mpago.la/iI5O";
                    } else if($deposito == 20) {
                      $cantidad = "http://mpago.la/GzxF";
                    } else if($deposito == 30) {
                      $cantidad = "http://mpago.la/6V2D";
                    } else if($deposito == 40) {
                      $cantidad = "http://mpago.la/wvBg";
                    } else if($deposito == 50) {
                      $cantidad = "http://mpago.la/YH5B";
                    } else if($deposito == 60) {
                      $cantidad = "http://mpago.la/X8or";
                    } else if($deposito == 70) {
                      $cantidad = "http://mpago.la/nMd8";
                    } else if($deposito == 80) {
                      $cantidad = "http://mpago.la/Xzlo";
                    } else if($deposito == 90) {
                      $cantidad = "http://mpago.la/3Wxh";
                    } else if($deposito == 10) {
                      $cantidad = "http://mpago.la/Gbak";
                    }

                    $iden = rand(0, 10000);

                    $conexion->query("INSERT INTO deposito (iden, correo, deposito, extra, estado) VALUES ('$iden', '$usuario', '$deposito', '$cantidad', '0')");

                    header("Location: depositos-pendientes.php");
                  }
                ?>
              </form>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
<?php
  ob_end_flush();
?>
