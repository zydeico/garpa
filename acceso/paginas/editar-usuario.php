<!-- === DATOS DE LA PAGINA === -->
<?php
  ob_start();
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if(empty($_GET) || !isset($_GET)) {
    header("Location: usuarios.php");
  }

  if($info['rango'] == 0) {
    header("Location: inicio.php");
  }

  $ucon2 = $conexion->query("SELECT * FROM usuarios WHERE correo='".$_GET['miembro']."'");

  if(mysqli_num_rows($ucon2) == 0) {
    header("Location: usuarios.php");
  } else {
    $info2 = mysqli_fetch_array($ucon2);
  }

  $titulo = "Editar miembro ".$info2['nombre']." ".$info2['apellido']." | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-exclamation"></i>Miembro <?php echo $info2['nombre']; echo ' '; echo $info2['apellido']; ?></h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <!-- Main content -->
      <section class="content container-fluid">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form class="form-horizontal" method="post" enctype="multipart/form-data">
                <center>
                  <img height="180" width="180" class="rounded-circle" src="<?php if(strpos($info2['foto'], 'https://') !== false) { echo $lconfig['foto']; } else { echo '../../'; echo $lconfig['ruta_file']; echo $info2['foto']; } ?>">
                  <br><br>
                  <div class="form-group">
                    <input type="file" name="fotop" id="fotop" class="form-control">
                  </div>
                </center>

                <div class="form-row mb-3 mt-5">
                  <div class="form-group col-md-6">
                    <label for="">Nombre</label>
                    <input type="text" name="nombre" class="form-control" value="<?php echo $info2['nombre']; ?>" placeholder="Nombre">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="">Apellido</label>
                    <input type="text" name="apellido" class="form-control" value="<?php echo $info2['apellido']; ?>" placeholder="Apellido">
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="">Correo Electronico</label>
                    <input type="email" name="correo" value="<?php echo $info2['correo']; ?>" class="form-control">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="">Direccion</label>
                    <input type="text" name="direccion" value="<?php echo $info2['direccion']; ?>" class="form-control" placeholder="Direccion">
                  </div>
                </div>

                <div class="form-group col-md-6">
                  <label for="">Contraseña</label>
                  <input type="password" name="contra" class="form-control" placeholder="Nueva contraseña">
                </div>

                <div class="form-group">
                  <label for="">Contraseña actual</label>
                  <input type="password" name="contra" class="form-control" placeholder="Contraseña actual para confirmar los cambios">
                </div>

                <button type="submit" name="cambiar" class="btn btn-primary btn-block">Actualizar</button>

                <?php
                  if(isset($_POST['cambiar'])) {
                    $fnombre = $_FILES['fotop']['name'];

                    $nombre = $_POST['nombre'];
                    $apellido = $_POST['apellido'];
                    $correo = $_POST['correo'];
                    $direccion = $_POST['direccion'];
                    $contra = $_POST['contra'];

                    if(preg_match("/[^aA-Zz]/", $nombre)
                    || preg_match("/[^aA-Zz]/", $apellido)
                    || preg_match("/^[0-9a-zA-Z]+$/", $direccion)
                    || preg_match("/^[0-9a-zA-Z]+$/", $correo)
                    || preg_match("/^[0-9a-zA-Z]+$/", $contra)) {
                      if( $nombre != '' || $apellido != '' || $direccion != '' || $correo != '' || $contra != '') {
                        if( !empty($contra)) {
                          $conexion->query("UPDATE usuarios SET contrasena='$contra' WHERE correo='".$info2['correo']."'");
                        }

                        $conexion->query("UPDATE usuarios SET nombre='$nombre', apellido='$apellido', correo='$correo', direccion='$direccion' WHERE correo='".$info2['correo']."'");

                        echo '<div class="fixed-bottom">
                          <div class="alert alert-success alert-dismissible fade show float-right" role="alert">
                            Datos actualizados
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                        </div>';
                      } else {
                        echo '<div class="fixed-bottom">
                          <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                            Faltan campos sin completar
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                        </div>';
                      }
                    } else {
                      echo '<div class="fixed-bottom">
                        <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                          Datos no validos
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                      </div>';
                    }

                    if( $info2['contrasena'] == $acontra ) {
                      if ( ($fnombre == !NULL) && ($_FILES['fotop']['size'] < $lconfig['subida']) ) {
                        if (($_FILES["fotop"]["type"] == "image/jpeg")
                         || ($_FILES["fotop"]["type"] == "image/jpg")
                         || ($_FILES["fotop"]["type"] == "image/png")) {
                           $direcciona = $_SERVER['DOCUMENT_ROOT'].$lconfig['ruta_root'];

                            $random = rand(0, 1000000);

                            move_uploaded_file($_FILES['fotop']['tmp_name'],$direcciona.$random.".png");

                            $ruta = "../../".$lconfig['ruta_file'];
                            $ffoto = $info2['foto'];

                            if( file_exists($ruta.$ffoto) ) {
                                unlink($ruta.$ffoto);
                            }

                            $conexion->query("UPDATE usuarios SET foto='$random.png' WHERE correo='".$info2['correo']."'");
                         } else {
                           echo '<div class="fixed-bottom">
                              <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                                No se puede subir una imagen con ese formato
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                            </div>';
                         }
                      } else {
                        if($fnombre == !NULL) {
                           echo '<div class="fixed-bottom">
                             <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                               La imagen es demasiado grande
                               <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">&times;</span>
                               </button>
                             </div>
                           </div>';
                         }
                      }
                    } else {
                      echo '<div class="fixed-bottom">
                          <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                            La contraseña es incorrecta
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                        </div>';
                    }

                    header("Refresh: 1; URL=editar-usuario.php?miembro".$_GET['miembro']);
                  }
                ?>
              </form>
              <p class="mb-5 mt-5">Si la foto de perfil contiene logos, marcas, paisajes sin rostros, será removida y reportado al administrador correspondiente.</p>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
<?php
  ob_end_flush();
?>
