<!-- === DATOS DE LA PAGINA === -->
<?php
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 0) {
    $dcon = $conexion->query("SELECT * FROM deposito WHERE correo='".$_SESSION['user']."'");
  } else {
    $dcon = $conexion->query("SELECT * FROM deposito");
  }

  $active_deposito = "active";
  $titulo = "Depositos Pendientes | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="icono-arg-factor-pago"></i> Depositos pendientes</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-primary card-outline">
                <div class="card-body">
                  <table class="table table-bordered">
                      <tbody>
                        <tr class="bg-dark text-white">
                          <?php if($info['rango'] == 1) { ?>
                            <th>Correo electronico</th>
                          <?php } ?>
                          <th>Dinero a depositar</th>
                          <th>Estado</th>
                          <th>Acciones</th>
                        </tr>
                        <?php if(mysqli_num_rows($dcon)>0) { ?>
                          <?php while($dinfo = mysqli_fetch_array($dcon)) { ?>
                            <?php
                              if($info['rango'] == 1) {
                                $icon = $conexion->query("SELECT * FROM usuarios WHERE correo='".$dinfo['correo']."'");
                                $iinfo = mysqli_fetch_array($icon);
                              }

                              if($dinfo['estado'] == 0) {
                                $estado = 0;
                              } else if($dinfo['estado'] == 1) {
                                $estado = 1;
                              }
                            ?>
                            <tr>
                              <?php if($info['rango'] == 1) { ?>
                                <td><?php echo $dinfo['correo']; ?></td>
                              <?php } ?>
                              <td><?php echo $dinfo['deposito']; ?></td>
                              <td>
                                <?php if($dinfo['estado']==0) { ?>
                                  <a href="#" class="btn btn-info" title="Pendiente"><i class="icono-arg-chequeado"></i></a>
                                <?php } else if($dinfo['estado']==1) { ?>
                                  <a href="#" class="btn btn-success" title="Aceptado"><i class="icono-arg-chequeado"></i></a>
                                <?php } else if($dinfo['estado']==2) { ?>
                                  <a href="#" class="btn btn-danger" title="Rechazado"><i class="icono-arg-reciclaje-basura"></i></a>
                                <?php } ?>
                              </td>
                              <td>
                                <?php if($info['rango'] == 1) { ?>
                                  <a href="<?php if($estado == 0) { ?>../acciones.php?deposito=<?php echo $dinfo['iden']; ?>&rechazar<?php } else { echo '#'; } ?>" class="btn btn-<?php if($estado == 0){ echo 'danger'; } else { echo 'secondary'; } ?>" title="Rechazar deposito de <?php echo $iinfo['nombre']; echo ' '; echo $iinfo['apellido']; echo ' / '; echo $iinfo['correo']; ?>" <?php if($estado == 0) { ?>onclick="return confirm('¿Desea rechazar el deposito de <?php echo $iinfo['nombre']; echo ' '; echo $iinfo['apellido']; echo ' / '; echo $iinfo['correo']; ?>?')"<?php } ?>>Rechazar</a>
                                  <a href="<?php if($estado == 0) { ?>../acciones.php?deposito=<?php echo $dinfo['iden']; ?>&aceptar<?php } else { echo '#'; } ?>" class="btn btn-<?php if($estado == 0){ echo 'primary'; } else { echo 'secondary'; } ?>" title="Aceptar deposito de <?php echo $iinfo['nombre']; echo ' '; echo $iinfo['apellido']; echo ' / '; echo $iinfo['correo']; ?>" <?php if($estado == 0) { ?>onclick="return confirm('¿Desea aceptar el deposito de <?php echo $iinfo['nombre']; echo ' '; echo $iinfo['apellido']; echo ' / '; echo $iinfo['correo']; ?>?')"<?php } ?>>Aceptar</a>
                                <?php } else { ?>
                                  <a <?php if($estado == 0) { ?> target="_blank" href="<?php echo $dinfo['extra']; ?>" <?php } else { ?> href="#" <?php } ?> title="Depositar Ahora" class="btn btn-<?php if($estado == 0) { echo 'primary'; } else { echo 'secondary'; } ?>">Depositar</a>
                                <?php } ?>
                              </td>
                            </tr>
                          <?php } ?>
                        <?php } else { ?>
                          <?php if($info['rango'] == 0) { ?>
                            <h2 class="text-center mb-3">No haz echo ningun deposito.</h2>
                          <?php } else { ?>
                            <h2 class="text-center mb-3">No hay depositos pendientes</h2>
                          <?php } ?>
                        <?php } ?>
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
