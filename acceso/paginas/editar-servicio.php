<!-- === DATOS DE LA PAGINA === -->
<?php
  ob_start();
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 0) {
    header("Location: inicio.php");
  }

  if(empty($_GET) || !isset($_GET)) {
    header("Location: usuarios.php");
  }

  $scon = $conexion->query("SELECT * FROM servicios WHERE id='".$_GET['servicio']."'");

  if(mysqli_num_rows($scon) == 0) {
    header("Location: agregar-servicio.php");
  } else {
    $sinfo = mysqli_fetch_array($scon);
  }

  $titulo = "Editar servicio ".$sinfo['nombre']." | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-exclamation"></i> Servicio <?php echo $sinfo['nombre']; ?></h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form method="post">
                <div class="form-group">
                  <label for="">Nombre</label>
                  <input type="text" name="snombre" class="form-control" value="<?php echo $sinfo['nombre']; ?>" placeholder="Nombre del servicio">
                </div>
                <div class="form-group">
                  <label for="">Descripcion</label>
                  <input type="text" name="sdescripcion" class="form-control" value="<?php echo utf8_encode($sinfo['descripcion']); ?>" placeholder="Descripcion del servicio">
                </div>
                <div class="form-group">
                  <label for="">Icono</label>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1">icono-arg-</span>
                    </div>
                    <input type="text" name="sicono" class="form-control" value="<?php echo $sinfo['icono'] ?>" placeholder="Icono para el servicio">
                  </div>
                </div>
                <div class="form-group">
                  <label for="">Enlace</label>
                  <input type="text" name="senlace" class="form-control" value="<?php echo $sinfo['enlace']; ?>" placeholder="Enlace para el servicio">
                </div>
                <select class="form-control" name="sestado">
                  <?php if($sinfo['estado'] == 0) { ?>
                    <option value="0">Desactivado</option>
                    <option value="1">Activado</option>
                  <?php } else { ?>
                    <option value="1">Activado</option>
                    <option value="0">Desactivado</option>
                  <?php } ?>
                </select>
                <button type="submit" name="actualizar" class="mt-3 mb-3 btn btn-success btn-block">Guardar</button>
                <?php
                  if(isset($_POST['actualizar'])) {
                    $nombre = $_POST['snombre'];
                    $descripcion = utf8_decode($_POST['sdescripcion']);
                    $icono = $_POST['sicono'];
                    $enlace = $_POST['senlace'];
                    $estado = $_POST['sestado'];

                    $conexion->query("UPDATE servicios SET nombre='$nombre', descripcion='$descripcion', icono='$icono', enlace='$enlace', estado='$estado' WHERE id='".$_GET['servicio']."'");
                    header("Refresh: 1; URL=editar-servicio.php?servicio=".$_GET['servicio']);
                  }
                ?>
              </form>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
<?php
  ob_end_flush();
?>
