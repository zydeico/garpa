<!-- === DATOS DE LA PAGINA === -->
<?php
  ob_start();
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 0) {
    header("Location: inicio.php");
  }

  $titulo = "Agregar servicio | Garpa Fácil";

  $servicioscon = $conexion->query("SELECT * FROM servicios ORDER BY id DESC");
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-plus"></i> Agregar servicio</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form method="post">
                <div class="form-group">
                  <label for="exampleInputPassword1">Nombre</label>
                  <input type="text" class="form-control" name="nombre" id="exampleInputPassword1" placeholder="Nombre del servicio.">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Descripción</label>
                  <input type="text" class="form-control" name="descripcion" id="exampleInputPassword1" placeholder="Descripción del servicio.">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Icono</label>
                  <input type="text" class="form-control" name="icono" id="exampleInputPassword1" placeholder="Ingrese la clase del icono.">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Enlace</label>
                  <input type="text" class="form-control" name="enlace" id="exampleInputPassword1" placeholder="Ingrese URL.">
                </div>
                <button type="submit" name="agregar" class="btn btn-primary btn-block btn-lg">Crear servicio</button>
                <?php
                  if(isset($_POST['agregar'])) {
                    $nombre = $_POST['nombre'];
                    $descripcion = utf8_decode($_POST['descripcion']);
                    $icon = $_POST['icono'];
                    $enlace = $_POST['enlace'];

                    $conexion->query("INSERT INTO servicios (nombre, descripcion, icono, enlace, estado) VALUES ('$nombre', '$descripcion', '$icon', '$enlace', '0')");
                    Header("Refresh: 1; URL=agregar-servicio.php");
                  }
                ?>
              </form>
            </div>
          </div>

          <br>

          <div class="card card-primary card-outline">
            <div class="card-body">
              <table class="table table-bordered mb-5">
                <tbody>
                  <tr class="bg-dark text-white">
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>URL</th>
                    <th>Estado</th>
                    <th>Editar</th>
                  </tr>
                  <?php if(mysqli_num_rows($servicioscon)>0) { ?>
                    <?php while($sinfo = mysqli_fetch_array($servicioscon)) { ?>
                      <?php
                        if($sinfo['estado'] == 0) {
                          $estado = "Desactivado";
                        } else if($sinfo['estado'] == 1) {
                          $estado = "Activado";
                        }
                      ?>
                      <tr>
                        <td><?php echo $sinfo['nombre']; ?></td>
                        <td><?php echo utf8_encode($sinfo['descripcion']); ?></td>
                        <td><?php echo $sinfo['enlace']; ?></td>
                        <td><?php echo $estado; ?></td>
                        <td>
                            <a href="../acciones.php?servcio=<?php echo $sinfo['id']; ?>" class="btn btn-danger"><i class="icono-arg-reciclaje-basura"></i></a>
                            <a href="editar-servicio.php?servicio=<?php echo $sinfo['id'] ?>" class="btn btn-success"><i class="icono-arg-escritura"></i></a>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } else { ?>
                    <h2 class="text-center mb-3">No hay servicios</h2>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
<?php
  ob_end_flush();
?>
