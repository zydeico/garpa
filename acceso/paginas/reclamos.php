<!-- === DATOS DE LA PAGINA === -->
<?php
  ob_start();
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 1) {
    header("Location: inicio-admin.php");
  }

  $titulo = "Reclamos | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-exclamation"></i> Reclamos</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form>
                <div class="form-group">
                  <label for="exampleInputPassword1">Número de telefono</label>
                  <input type="text" class="form-control" name="numero" id="exampleInputPassword1" placeholder="Ingrese número de telefono o celular.">
                  <small id="" class="form-text text-muted">Caracteristica sin el 0 + numero sin el 15.</small>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Detalles</label>
                  <small id="" class="form-text text-muted">Detalle que incovenientes tiene.</small>
                  <textarea class="form-control" name="detalle" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <p>Su reclamo será atendido mediante una llamada o WhatsApp, es importante que ingrese correctamente su número de telefono.</p>
                <button type="submit" name="enviar" class="btn btn-primary btn-block btn-lg">Enviar reclamo</button>
                <?php
                  if(isset($_POST['enviar'])) {
                    $numero = $_POST['numero'];
                    $detalle = $_POST['detalle'];
                    $orreo = $info['correo'];

                    $conexion->query("INSERT INTO reclamos (correo, numero, detalles) VALUES ('$correo', '$numero', '$detalle')");
                    header("Refresh: 1; URL=reclamos.php?exito");
                  }
                ?>
              </form>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>

    <?php
      if(isset($_GET['exito'])) {
        echo '<div class="fixed-bottom">
          <div class="alert alert-success alert-dismissible fade show float-right" role="alert">
            El reclamo fue enviado con exito
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        </div>';

        header("Refresh: 1; URL=reclamos.php");
      }
    ?>

  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
<?php
  ob_end_flush();
?>
