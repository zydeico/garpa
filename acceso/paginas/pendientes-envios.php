<!-- === DATOS DE LA PAGINA === -->
<?php
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 1) {
    header("Location: inicio-admin.php");
  }

  $econ = $conexion->query("SELECT * FROM envio WHERE correo='".$_SESSION['user']."'");

  $active_enviados = "active";
  $titulo = "Pendientes de envio | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="icono-arg-envio-terrestre"></i> Envios pendientes</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="card card-primary card-outline">
          <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                  <tr class="bg-dark text-white">
                    <th>Fecha</th>
                    <th>Dirección</th>
                    <th>Envio rápido</th>
                    <th>Servicio</th>
                    <th>Progreso de envio</th>
                    <th>Acciones</th>
                  </tr>
                  <?php if(mysqli_num_rows($econ)>0) { ?>
                    <?php while($einfo = mysqli_fetch_array($econ)) { ?>
                      <?php
                        if($einfo['tipo'] == 0) {
                          $tipo = "No";
                        } else {
                          $tipo = "Si";
                        }
                      ?>
                      <tr>
                        <td><?php echo $einfo['fecha']; ?></td>
                        <td><?php echo $einfo['direccion']; ?></td>
                        <td><?php echo $tipo; ?></td>
                        <td><?php echo $einfo['servicio']; ?></td>
                        <td>
                          <div class="progress progress-xs">
                            <div class="progress-bar bg-warning" style="width: <?php echo $einfo['progreso']; ?>%"></div>
                          </div>
                        </td>
                        <td>
                            <a href="../acciones.php?cancelar=<?php echo $einfo['iden']; ?>" class="btn btn-danger" title="Cancelar envio <?php echo $einfo['servicio']; ?>" onclick="return confirm('¿Cancelar envio <?php echo $einfo['servicio']; ?>?)"><i class="fa fa-close"></i> Cancelar</a>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } else { ?>
                    <h2 class="text-center mb-3">No haz pedido ningun envio</h2>
                  <?php } ?>
                </tbody>
              </table>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
