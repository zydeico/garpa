<!-- === DATOS DE LA PAGINA === -->
<?php
$titulo = "Inicio | Earth Is Water";
$active_home = "active";
 ?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-dashboard"></i> Panel general</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">

        <div class="row">

          <div class="col-md-12">
            <div class="card card-primary card-outline">
              <div class="card-body">
                <div class="row">

                  <div class="col-md-6">
                      <div class="small-box bg-dark">
                        <div class="inner">
                          <h3>150</h3>

                          <p>Reportes totales</p>
                        </div>
                        <div class="icon text-white">
                          <i class="fa fa-bar-chart"></i>
                        </div>
                      </div>
                  </div>

                  <div class="col-md-6">
                      <div class="small-box bg-primary">
                        <div class="inner">
                          <h3>50</h3>

                          <p>Reportes de tu club náutico</p>
                        </div>
                        <div class="icon text-white">
                          <i class="fa fa-bar-chart"></i>
                        </div>
                      </div>
                  </div>

              </div>
            </div>

          </div>
        </div>

        </div>

        <div class="row">

          <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                    <a href="../img/avatar5.png" data-fancybox="images" data-caption="img/avatar5.png">
                      <img class="profile-user-img img-fluid img-circle" src="../img/avatar5.png" alt="User profile picture">
                    </a>
                </div>

                <h3 class="profile-username text-center">Alexander Pierce</h3>

                <p class="text-muted text-center"><b>Asociación Cordobesa del remo</b></p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Reportes</b> <a class="float-right">1,322</a>
                  </li>
                  <li class="list-group-item">
                    <b>Aceptados</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Rechazados</b> <a class="float-right">17</a>
                  </li>
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>

          <div class="col-md-9">

              <div class="card card-primary card-outline">
                <div class="card-body">
                  <h3>Lagos asignados</h3>
                  <hr>

                  <div class="row">
                    <div class="col-md-4 mt-4">
                      <div class="card">
                        <a href="https://i.imgur.com/rO7l731.jpg " data-fancybox="images" data-caption="https://i.imgur.com/rO7l731.jpg">
                        <img style="width:100%; max-height:315px;" class="" src="https://i.imgur.com/rO7l731.jpg" alt="https://i.imgur.com/rO7l731.jpg">
                        </a>
                          <div class="card-body">
                            <h5 class="card-title"><b>Lago los Molinos</b></h5>
                            <p class="card-text">Provincia de Córdoba</p>
                            <a href="reporte-user.php" class="btn btn-primary btn-block">Reportar</a>
                          </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
          </div>

        </div>

      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>


</html>
