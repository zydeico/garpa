<!-- === DATOS DE LA PAGINA === -->
<?php
$titulo = "Agregar lago | Earth Is Water";
 ?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-edit"></i> Agregar lago</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
          <div class="card card-primary card-outline">
            <div class="card-body">

                <div class="form-group">
                  <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese el nombre del lago">
                </div>
                 <hr>
                 <div class="row mt-3">

                   <div class="col-md-6">
                       <div class="card">
                         <div class="card-header bg-dark text-white">
                           País
                         </div>
                           <div class="card-body">
                             <div class="form-group">
                               <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese el país">
                             </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-6">
                       <div class="card">
                         <div class="card-header bg-dark text-white">
                           Estado o provincia
                         </div>
                           <div class="card-body">
                             <div class="form-group">
                               <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese estado o provincia">
                             </div>
                           </div>
                       </div>
                   </div>

                 </div>

                 <div class="row mt-3">

                   <div class="col-md-4">
                       <div class="card">
                         <div class="card-header bg-dark text-white">
                           Condición del lago
                         </div>
                           <div class="card-body">
                             <div class="form-group">
                               <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese su condición">
                             </div>
                           </div>
                       </div>
                   </div>
                   <div class="col-md-4">
                       <div class="card">
                         <div class="card-header bg-dark text-white">
                           Densidad Poblacional
                         </div>
                           <div class="card-body">
                             <div class="form-group">
                               <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese densidad poblacional">
                             </div>
                           </div>
                       </div>
                   </div>

                   <div class="col-md-4">
                       <div class="card">
                         <div class="card-header bg-dark text-white">
                           Ciudad principal
                         </div>
                           <div class="card-body">
                             <div class="form-group">
                               <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese la ciudad principal">
                             </div>
                           </div>
                       </div>
                   </div>

                 </div>

                 <div class="row mt-3">

                   <div class="col-md-12">
                       <div class="card">
                         <a class="card-header bg-dark text-white" data-toggle="collapse" href="#resumen" role="button" aria-expanded="false" aria-controls="collapseExample">
                           Resumen
                         </a>
                           <div class="card-body collapse" id="resumen">
                           <textarea class="form-control" style="width:100%; height:10rem;">Descripción
                           </textarea>
                           </div>
                       </div>
                   </div>

                 </div>

                 <div class="row mt-3">

                   <div class="col-md-12">
                       <div class="card">
                         <a class="card-header bg-dark text-white" data-toggle="collapse" href="#datos-adicionales" role="button" aria-expanded="false" aria-controls="collapseExample">
                           Datos adicionales
                         </a>
                           <div class="card-body collapse" id="datos-adicionales">
                             <div class="form-group">
                               <label>Altura</label>
                               <input type="text" class="form-control" id="Altura" value="" placeholder="Ingrese altura">
                             </div>
                             <div class="form-group">
                               <label>Cuenca hidrográfica</label>
                               <input type="text" class="form-control" id="Altura" value="" placeholder="Ingrese Cuenca hidrográfica">
                             </div>
                             <div class="form-group">
                               <label>Superficie‎</label>
                               <input type="text" class="form-control" id="Altura" value="" placeholder="Ingrese Superficie‎">
                             </div>
                             <div class="form-group">
                               <label>Capacidad total</label>
                               <input type="text" class="form-control" id="Altura" value="" placeholder="Ingrese Capacidad total">
                             </div>
                           </div>
                       </div>
                   </div>

                 </div>

                 <div class="row mt-3">

                   <div class="col-md-6">
                       <div class="card">
                         <a class="card-header bg-dark text-white" data-toggle="collapse" href="#mapadatos" role="button" aria-expanded="false" aria-controls="collapseExample">
                           Mapa de datos
                         </a>
                           <div class="card-body collapse" id="mapadatos">
                             <div class="form-group">
                               <label>Url de Google Maps</label>
                               <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese Url">
                             </div>
                           </div>
                       </div>
                   </div>

                   <div class="col-md-6">
                       <div class="card">
                         <a class="card-header bg-dark text-white" data-toggle="collapse" href="#satelites" role="button" aria-expanded="false" aria-controls="collapseExample">
                           Satelites
                         </a>
                           <div class="card-body collapse" id="satelites">
                             <div class="form-group">
                               <label>Lansat 8 - URL de <a href="https://eos.com/landviewer/">Landviewer</a></label>
                               <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese URL Landsat 8">
                             </div>
                             <div class="form-group">
                               <label>Lansat 8 - URL de <a href="https://eos.com/landviewer/">Landviewer</a></label>
                               <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese URL Landsat 7">
                             </div>
                             <div class="form-group">
                               <label>Sentinel 2 - URL de <a href="https://eos.com/landviewer/">Landviewer</a></label>
                               <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese URL Sentinel 2">
                             </div>
                           </div>
                       </div>
                   </div>

                 </div>

                 <div class="row mt-3">

                   <div class="col-md-12">
                         <div class="card">
                           <a class="card-header bg-dark text-white" data-toggle="collapse" href="#meses" role="button" aria-expanded="false" aria-controls="collapseExample">
                             Historial de medidas
                           </a>
                             <div class="card-body collapse" id="meses">
                               <div class="form-group">
                                 <label>Enero</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Enero">
                               </div>
                               <div class="form-group">
                                 <label>Febrero</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Febrero">
                               </div>
                               <div class="form-group">
                                 <label>Marzo</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Marzo">
                               </div>
                               <div class="form-group">
                                 <label>Abril</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Abril">
                               </div>
                               <div class="form-group">
                                 <label>Mayo</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Mayo">
                               </div>
                               <div class="form-group">
                                 <label>Junio</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Junio">
                               </div>
                               <div class="form-group">
                                 <label>Julio</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Julio">
                               </div>
                               <div class="form-group">
                                 <label>Agosto</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Agosto">
                               </div>
                               <div class="form-group">
                                 <label>Septiembre</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Septiembre">
                               </div>
                               <div class="form-group">
                                 <label>Octubre</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Octubre">
                               </div>
                               <div class="form-group">
                                 <label>Noviembre</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Noviembre">
                               </div>
                               <div class="form-group">
                                 <label>Diciembre</label>
                                 <input type="text" class="form-control" id="Url" value="" placeholder="Ingrese medida TSI Diciembre">
                               </div>
                             </div>
                         </div>
                   </div>

                 </div>

                 <br>
                 <div class="row">
                   <div class="col-md-6">
                     <a href="#" class="btn btn-primary btn-block text-white">Guardar cambios</a>
                   </div>
                   <div class="col-md-6">
                     <a href="inicio-admin.php" class="btn btn-info btn-block text-white">Regresar</a>
                   </div>
                 </div>

            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
