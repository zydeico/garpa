<!-- === DATOS DE LA PAGINA === -->
<?php
$titulo = "Inicio | Earth Is Water";
$active_home = "active";
 ?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
        <div class="row">

          <div class="col-md-12">
            <div class="mb-2">
                <div class="card card-body">
                  <h1><i class="fa fa-dashboard"></i> Panel general</h1>
                </div>
            </div>
          </div>

        </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">

        <div class="row">
          <div class="col-md-3">
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                    <a href="img/avatar5.png" data-fancybox="images" data-caption="https://i.imgur.com/oUAhRu8.jpg">
                      <img class="profile-user-img img-fluid img-circle" src="https://i.imgur.com/oUAhRu8.jpg" alt="User profile picture">
                    </a>
                </div>
                <h3 class="profile-username text-center">Alexander Admin</h3>
                <p class="text-muted text-center"><b>Administrador/a</b></p>
              </div>
            </div>
          </div>

          <div class="col-md-9 mt-4">
            <div class="card card-primary card-outline">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                      <div class="small-box bg-dark">
                        <div class="inner">
                          <h3>150</h3>

                          <p>Reportes totales</p>
                        </div>
                        <div class="icon text-white">
                          <i class="fa fa-bar-chart"></i>
                        </div>
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="small-box bg-primary">
                        <div class="inner">
                          <h3>5</h3>

                          <p>Lagos activos</p>
                        </div>
                        <div class="icon text-white">
                          <i class="fa fa-bar-chart"></i>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-md-12">
              <div class="card card-primary card-outline">
                <div class="card-body">
                  <h3>Opciones administrativas</h3>
                  <hr>

                  <div class="row">

                    <div class="col-md-4">
                      <div class="card card-primary ">
                        <div class="card-header text-white" style="background-color:#111111; border-left: 6px solid red;">
                          <h6 class="m-0">Gestión de lagos</h6>
                        </div>
                        <div class="card-body">
                          <p class="card-text">
                            <b>6</b> Lagos Actuales<br>
                            <b>1</b> Lagos Activos<br>
                            <b>5</b> Lagos Inactivos</p>
                          <a href="#" class="btn btn-primary btn-block">Acceder</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card card-primary ">
                        <div class="card-header text-white" style="background-color:#111111;  border-left: 6px solid red;">
                          <h6 class="m-0">Gestión de reportes</h6>
                        </div>
                        <div class="card-body">
                          <p class="card-text">
                            <b>150</b> Reportes actuales<br>
                            <b>0</b> Reportes aceptados<br>
                            <b>0</b> Reportes rechazados</p>
                          <a href="#" class="btn btn-primary btn-block">Acceder</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card card-primary ">
                        <div class="card-header text-white" style="background-color:#111111;  border-left: 6px solid red;">
                          <h6 class="m-0">Gestión de Usuarios</h6>
                        </div>
                        <div class="card-body">
                          <p class="card-text">
                            <b>2</b> Administradores<br>
                            <b>5</b> Clubes náuticos<br>
                            <b>20</b> Sub usuarios</p>
                          <a href="#" class="btn btn-primary btn-block">Acceder</a>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="row">

                    <div class="col-md-4">
                      <div class="card card-primary ">
                        <div class="card-header text-white" style="background-color:#111111; border-left: 6px solid red;">
                          <h6 class="m-0">Añadir Club Náutico</h6>
                        </div>
                        <div class="card-body">
                            <p>Aqui puede crear un Club náutico</p>
                          <a href="#" class="btn btn-primary btn-block">Acceder</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card card-primary ">
                        <div class="card-header text-white" style="background-color:#111111;  border-left: 6px solid red;">
                          <h6 class="m-0">Asignación de lagos</h6>
                        </div>
                        <div class="card-body">
                          <p>Aqui podras asignar el lago a cada club náutico</p>
                          <a href="#" class="btn btn-primary btn-block">Acceder</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="card card-primary ">
                        <div class="card-header text-white" style="background-color:#111111;  border-left: 6px solid red;">
                          <h6 class="m-0">Gestión de avisos</h6>
                        </div>
                        <div class="card-body">
                          <p>Genere avisos por cada lago de su condición o para generar conciencia del mismo</p>
                          <a href="avisos.php" class="btn btn-primary btn-block">Acceder</a>
                        </div>
                      </div>
                    </div>

                  </div>

                </div>
              </div>
          </div>
        </div>

      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
