<!-- === DATOS DE LA PAGINA === -->
<?php
$titulo = "Avisos | Earth Is Water";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-picture-o"></i> Avisos</h1><br>
                 <small>Estos avisos se muestran en la página principal de cada lago.</small>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form class="form-horizontal">

                <div class="form-group">
                  <label for="discosecchi">Subir imagen para banner</label>
                  <input type="file" accept="image/*" name="imagen" id="capture" capture="camera" required>
                </div>
                <p>Si la foto de perfil contiene marcas registradas sin autorización o visualización de menores sin autorización legal, será removida y reportada.</p>

                <div class="form-group">
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block">Subir</button>
                  </div>
                </div>
              </form>

            </div>
          </div>

          <div class="row">
            <div class="col-md-12">

              <div class="card card-primary card-outline mt-2">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-2">
                      <a href="https://i.imgur.com/CfyFIub.jpg" data-fancybox="images" data-caption="Banner 1">
                      <img src="https://i.imgur.com/CfyFIub.jpg" alt="" style="width:100%;">
                      </a>
                    </div>
                    <div class="col-md-10">
                      <b>Banner 1</b><br>
                      <a href="#" class="btn btn-danger btn-block btn-sm">Eliminar</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="card card-primary card-outline mt-2">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-2">
                        <a href="https://i.imgur.com/CfyFIub.jpg" data-fancybox="images" data-caption="Banner 2">
                        <img src="https://i.imgur.com/CfyFIub.jpg" alt="" style="width:100%;">
                        </a>
                      </div>
                      <div class="col-md-10">
                        <b>Banner 2</b><br>
                        <a href="#" class="btn btn-danger btn-block btn-sm">Eliminar</a>
                      </div>
                    </div>
                  </div>
              </div>

            </div>
          </div>

      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
