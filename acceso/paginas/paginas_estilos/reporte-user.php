<!-- === DATOS DE LA PAGINA === -->
<?php
$titulo = "Reporte | Earth Is Water";
$active_reporte = "active";
 ?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-exclamation"></i> Reportar lago (pais/nombre del lago)</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">

          <div class="card card-primary card-outline">
            <div class="card-body">

              <form class="" action="index.html" method="post">
                <div class="form-group has-success">
                    <label class="control-label" for="coordenadas-reporte"><i class="fa fa-map"></i> Coordenadas <a href="#coordenadas" id="obtener" role="button" data-toggle="modal" class="btn btn-primary btn-sm">Obtener mi localización</a></label>
                    <input type="text" class="form-control" id="coordenadas-reporte" placeholder="Ingresar coordenadas" required>
                </div>
                <div class="form-group has-success">
                    <label class="control-label" for="embarcacion"><i class="fa fa-ship"></i> Embarcación</label>
                    <input type="text" class="form-control" id="embarcacion" placeholder="Ingresar matricula o nombre" required>
                </div>
                <div class="form-group has-success">
                    <label class="control-label" for="discosecchi"><i class="fa fa-check-square-o"></i> Medida de disco secchi (en cm)</label>
                    <input type="text" class="form-control" id="discosecchi" placeholder="Ingresar medida" required>
                </div>
                <div class="form-group">
                  <label for="discosecchi">Imagen</label>
                  <input type="file" accept="image/*" name="imagen" id="capture" capture="camera">
                </div>
                <button type="submit" class="btn btn-primary btn-block">Subir reporte</button>
              </form>

            </div>
          </div>

      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>
  <!-- Coordenadas -->
  <div id="coordenadas" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-dark">
          <h5 class="modal-title text-white"><i class="fa fa-map"></i> Coordenadas generadas <button type="button" class="btn btn-danger btn-sm pull-right"  data-dismiss="modal">Cerrar</button></h5>
        </div>
        <div class="modal-body bg-white text-dark">
          <h5>Copie y pegue las coordenadas generadas</h5>
          <div id="localizacion">

          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
  <script type="text/javascript">
  function iniciar(){
  var boton=document.getElementById('obtener');
  boton.addEventListener('click', obtener, false);
  }

  function obtener(){navigator.geolocation.getCurrentPosition(mostrar, gestionarErrores);}

  function mostrar(posicion){
  var ubicacion=document.getElementById('localizacion');
  var datos='';
  datos+='Latitud: '+posicion.coords.latitude+'<br>';
  datos+='Longitud: '+posicion.coords.longitude+'<br>';
  datos+='Exactitud: '+posicion.coords.accuracy+' metros.<br>';
  ubicacion.innerHTML=datos;
  }

  function gestionarErrores(error){
  alert('Error: '+error.code+' '+error.message+ 'nnPor favor compruebe que está conectado '+
  'a internet y habilite la opción permitir compartir ubicación física');
  }

  window.addEventListener('load', iniciar, false);

  </script>
</html>
