<!-- === DATOS DE LA PAGINA === -->
<?php
  ob_start();
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 1) {
    header("Location: inicio.php");
  }

  $titulo = "Enviar factura | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="icono-arg-envio-terrestre_1"></i> Enviar Factura</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="exampleInputEmail1">Dirección de envío</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" name="direccion" value="<?php echo $info['direccion']; ?>" placeholder="Ingrese su dirección">
                  <small id="" class="form-text text-muted">Ingrese todos los detalles (nombre de calle, numeración, piso o departamento)</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Referencias</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" name="referencia" placeholder="Ingrese referencias">
                  <small id="" class="form-text text-muted">Ingrese alguna referencia del lugar de envio.</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Servicios a imprimir</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" name="servicio" placeholder="Ingrese los servicios que desea imprimir.">
                  <small id="" class="form-text text-muted">Los servicios deben estar en el listado del inicio, separados por comas.</small>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Detalles adicionales.</label>
                  <small id="" class="form-text text-muted">Ingrese algunos los datos de acceso a las cuentas virtuales del servicio que desee imprimir o numeros de contrato de sus facturas, etc.. <br> para mas información <a href="datos.php">ingrese aqui.</a></small>
                  <textarea class="form-control" id="exampleFormControlTextarea1" name="detalles" rows="3"></textarea>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Archivos adicionales.</label>
                  <input type="file" name="archivos">
                  <small id="" class="form-text text-muted">PDF o archivos de texto de alguna factura para imprimir.</small>
                </div>
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" name="estadoa" value="1" id="defaultCheck1">
                  <label class="form-check-label" for="defaultCheck1">
                    Envio Rápido
                  </label>
                  <small id="" class="form-text text-muted">El servicio de envio estandar, suele demorar de 3 a 5 días<br>Si solicita el envio rápido tardará desde 24 a 48 horas con una carga adicional de 20$AR en la tarifa.</small>
                  <br>
                </div>
                <p>Según nuestro cuadro tarifario, requiere un minimo de 1 impuestos o facturaciones.</p>
                <ul>
                  <li>1 factura o cedulon - 38$</li>
                  <li>5 facturaciones o cedulones - 40$AR</li>
                  <li>10 facturaciones o cedulones - 60$AR</li>
                  <li>20 o mas - 80$AR</li>
                </ul>
                <button type="submit" name="enviar" class="btn btn-primary btn-block btn-lg">Enviar factura</button>

                <?php
                  if(isset($_POST['enviar'])) {
                    $correo = $info['correo'];
                    $direccion = $_POST['direccion'];
                    $referencia = $_POST['referencia'];
                    $servicio = $_POST['servicio'];
                    $detalles = $_POST['detalles'];

                    if(isset($_POST['estadoa'])) {
                      $envio = $_POST['estadoa'];
                    } else {
                      $envio = 0;
                    }

                    $archivo = $_FILES['archivos']['name'];

                    $iden = rand(0, 10000);

                    if(preg_match("/^[0-9a-zA-Z]+$/", $direccion)
                    || preg_match("/^[0-9a-zA-Z]+$/", $referencia)
                    || preg_match("/^[0-9a-zA-Z]+$/", $servicio)
                    || preg_match("/^[0-9a-zA-Z]+$/", $detalles)) {
                      if($direccion != '' || $referencia != '' || $servicio != '' || $detalles != '' ) {
                        if($envio == 1) {
                          $estado = 1;
                        } else {
                          $estado = 0;
                        }

                        date_default_timezone_set('America/Argentina/Buenos_Aires');

                        $fecha = date("h:i")." ".date("j/n/Y");

                        $conteo = explode(",", $servicio);
                        $ctotal = count($conteo);

                        if($ctotal == 20 || $ctotal >= 21) {
                          $tarifa = "80$";
                        } else if($ctotal == 10 || $ctotal == 11 || $ctotal == 12|| $ctotal == 13 || $ctotal == 14 || $ctotal == 15 || $ctotal == 16 || $ctotal == 17  || $ctotal == 18 || $ctotal == 19) {
                          $tarifa = "60$";
                        } else if($ctotal == 5 || $ctotal == 6 || $ctotal == 7 || $ctotal == 8 || $ctotal == 9) {
                          $tarifa = "40$";
                        } else if($ctotal == 0 || $ctotal == 1 || $ctotal == 2 || $ctotal == 3 || $ctotal == 4 ) {
                          $tarifa = "38$";
                        }

                        $conexion->query("INSERT INTO envio (iden, correo, direccion, referencia, servicio, tarifa, detalle, fecha, archivo, progreso, tipo, estado) VALUES ('$iden', '$correo', '$direccion', '$referencia', '$servicio', '$tarifa', '$detalles', '$fecha', '', '0', '$estado', '0')");

                        echo '<div class="fixed-bottom">
                           <div class="alert alert-success alert-dismissible fade show float-right" role="alert">
                             El pedido fue enviado
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                               <span aria-hidden="true">&times;</span>
                             </button>
                           </div>
                         </div>';

                        if ( ($archivo == !NULL) && ($_FILES['archivos']['size'] < $lconfig['subida']+$lconfig['subida']) ) {
                          if (($_FILES["archivos"]["type"] == "application/pdf")) {
                            $random = rand(0, 1000000);

                            $direcciona = $_SERVER['DOCUMENT_ROOT'].$lconfig['ruta_root'];

                            move_uploaded_file($_FILES['archivos']['tmp_name'],$direcciona.$random.".pdf");

                            $conexion->query("UPDATE envio SET archivo='$random.pdf' WHERE iden='$iden'");

                            $ruta = "../../".$lconfig['ruta_file'];
                          } else {
                            echo '<div class="fixed-bottom">
                               <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                                 Formato no permitodo solo pdf
                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                                 </button>
                               </div>
                             </div>';
                          }
                        } else {
                          if($archivo == !NULL) {
                             echo '<div class="fixed-bottom">
                               <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                                 EL archivo es demasiado grande
                                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                   <span aria-hidden="true">&times;</span>
                                 </button>
                               </div>
                             </div>';
                           }
                        }

                        header("Refresh: 1; URL=pendientes-envios.php");
                      } else {
                        echo '<div class="fixed-bottom">
                          <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                            Faltan campos sin completar
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                        </div>';
                      }
                    } else {
                      echo '<div class="fixed-bottom">
                        <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                          Datos no validos
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                      </div>';
                    }
                  }
                ?>
              </form>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
<?php
  ob_end_flush();
?>
