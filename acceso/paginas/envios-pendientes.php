<!-- === DATOS DE LA PAGINA === -->
<?php
  ob_start();
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 0) {
    header("Location: ../pendientes-envios.php");
  }

  $econ = $conexion->query("SELECT * FROM envio ORDER BY id DESC");

  $titulo = "Envios pendientes | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="icono-arg-envio-terrestre"></i> Envios pendientes</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="card card-primary card-outline">
          <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                  <tr class="bg-dark text-white">
                    <th>Fecha</th>
                    <th>Correo Electronico</th>
                    <th>Dirección</th>
                    <th>Envio rápido</th>
                    <th>Progreso de envio</th>
                    <th>Editar</th>
                  </tr>
                  <?php if(mysqli_num_rows($econ)>0) { ?>
                    <?php while($einfo = mysqli_fetch_array($econ)) { ?>
                      <?php
                        if($einfo['tipo'] == 1) {
                          $tipo = "Si";
                        } else if($einfo['tipo'] == 0) {
                          $tipo = "No";
                        }
                      ?>
                      <tr>
                        <td><?php echo $einfo['fecha']; ?></td>
                        <td><?php echo $einfo['correo']; ?></td>
                        <td><?php echo $einfo['direccion']; ?></td>
                        <td><?php echo $tipo; ?></td>
                        <td>
                          <div class="progress progress-xs">
                            <div class="progress-bar bg-warning" style="width: <?php echo $einfo['progreso']; ?>%"></div>
                          </div>
                        </td>
                        <td>
                          <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#e<?php echo $einfo['iden']; ?>" title="Editar"><i class="icono-arg-escritura"></i></a>
                          <?php
                            $ucon2 = $conexion->query("SELECT * FROM usuarios WHERE correo='".$einfo['correo']."'");
                            $uinfo2 = mysqli_fetch_array($ucon2);

                            if($einfo['archivo'] == '') {
                              $archivo = "Ninguno";
                            } else {
                              $archivo = "<a href='../../".$lconfig['ruta_file'].$einfo['archivo']."' target='_blank' class='btn btn-info btn-sm' title='Ver archivo'>".$einfo['archivo']."</a>";
                            }
                          ?>
                          <div class="modal fade" id="e<?php echo $einfo['iden']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header bg-dark text-white">
                                  <h5 class="modal-title" id="exampleModalLabel">Datos del envio <?php echo $einfo['iden']; ?></h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  <b>Dirección:</b> <?php echo utf8_encode($einfo['direccion']); ?><br>
                                  <b>Nombre y apellido:</b> <?php echo $uinfo2['nombre']; echo ' '; echo $uinfo2['apellido']; ?><br>
                                  <b>Tarifa:</b> <?php echo $einfo['tarifa']; ?> <br>
                                  <b>Método de pago:</b> Efectivo presencial <br>
                                  <b>Detalles (datos de ingreso para acceder facturación):</b> <?php echo utf8_encode($einfo['detalle']); ?><br>
                                  <b>Archivos:</b> <?php echo $archivo; ?><br>
                                  <b>Facturas a imprimir:</b> <?php echo $einfo['servicio']; ?><br>
                                  <b>Editar progreso:</b>
                                  <form method="post">
                                    <div class="form-group mb-3">
                                      <input type="text" class="form-control" name="progreso" value="<?php echo $einfo['progreso']; ?>" placeholder="Ingrese porcentaje">
                                    </div>
                                    <button type="submit" name="actualizar" class="btn btn-success btn-block">Actualizar</button>
                                    <?php
                                      if(isset($_POST['actualizar'])) {
                                        $progreso = $_POST['progreso'];
                                        $conexion->query("UPDATE envio SET progreso='$progreso' WHERE iden='".$einfo['iden']."'");
                                        if($progreso > 99) {
                                          header("Location: ../acciones.php?envio=".$einfo['iden']."&notificar=entregado");
                                        } else if($progreso > 89) {
                                          header("Location: ../acciones.php?envio=".$einfo['iden']."&notificar");
                                        } else {
                                          header("Refresh: 1; URL=envios-pendientes.php");
                                        }
                                      }
                                    ?>
                                  </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Cerrar</button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <a href="../acciones.php?envio=<?php echo $einfo['iden']; ?>" class="btn btn-danger" title="Borrar envio de <?php echo $uinfo2['nombre']; echo ' '; echo $uinfo2['apellido']; ?>"><i class="icono-arg-reciclaje-basura"></i></a>
                          <a href="../acciones.php?envio=<?php echo $einfo['iden']; ?>&notificar" class="btn btn-success"><i class="icono-arg-notificaciones"></i> Notificar</a>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } else { ?>
                    <h2 class="text-center mb-3">No hay pedidos pendientes</h2>
                  <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>
  <?php include('../php/footer.php'); ?>

</html>
<?php
  ob_end_flush();
?>
