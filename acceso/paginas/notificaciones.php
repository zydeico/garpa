<!-- === DATOS DE LA PAGINA === -->
<?php
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 1) {
    header("Location: inicio-admin.php");
  }

  $active_noti_envio = "active";
  $titulo = "Notificaciones | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-bell"></i> Notificaciones</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="card card-primary card-outline">
          <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                  <tr class="bg-dark text-white">
                    <th>Fecha</th>
                    <th>Razón</th>
                    <th>Editar</th>
                  </tr>
                  <?php if(mysqli_num_rows($ncon)>0) { ?>
                    <?php while($ninfo = mysqli_fetch_array($ncon)) { ?>
                      <?php
                        $conexion->query("UPDATE notificacion SET estado='1' WHERE estado='0' AND correo='".$info['correo']."'");
                      ?>
                      <tr>
                        <td><?php echo $ninfo['fecha']; ?></td>
                        <td><?php echo $ninfo['mensaje']; ?></td>
                        <td>
                            <a href="../acciones.php?aviso=<?php echo $ninfo['iden']; ?>" class="btn btn-danger"><i class="fa fa-close"></i> Eliminar aviso</a>
                        </td>
                      </tr>
                    <?php } ?>
                  <?php } else { ?>
                    <h2 class="text-center mb-3">No tienes ninguna notificacion</h2>
                  <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
