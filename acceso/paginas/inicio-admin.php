<!-- === DATOS DE LA PAGINA === -->
<?php
  ob_start();
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 0) {
    header("Location: ../index.php");
  }

  $ucon2 = $conexion->query("SELECT * FROM usuarios WHERE rango='0'");

  $econ = $conexion->query("SELECT * FROM envio WHERE estado='0'");

  $econ2 = $conexion->query("SELECT * FROM envio WHERE estado='0' AND tipo='1'");

  $dcon = $conexion->query("SELECT SUM(deposito) AS Total FROM usuarios WHERE rango='0'");

  $dcon2 = $conexion->query("SELECT * FROM deposito WHERE estado='0'");

  $pcon = $conexion->query("SELECT * FROM pagar WHERE estado='0'");

  $scon = $conexion->query("SELECT * FROM servicios WHERE estado='1'");

  $titulo = "Inicio Admin | Garpa Fácil";
  $active_inicio = "active";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-dashboard"></i> Inicio</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12">
                <div class="card card-primary card-outline">
                  <div class="card-body box-profile">

                    <div class="row">

                      <div class="col-md-3">
                        <div class="text-center">
                            <a href="<?php if(strpos($info['foto'], 'https://') !== false) { echo $lconfig['foto']; } else { echo '../../'; echo $lconfig['ruta_file']; echo $info['foto']; } ?>" data-fancybox="images" data-caption="<?php echo $info['nombre']; echo ' '; echo $info['apellido']; ?>">
                              <img class="profile-user-img img-fluid img-circle" src="<?php if(strpos($info['foto'], 'https://') !== false) { echo $lconfig['foto']; } else { echo '../../'; echo $lconfig['ruta_file']; echo $info['foto']; } ?>" alt="User profile picture">
                            </a>
                        </div>
                        <h3 class="profile-username text-center"><?php echo $info['nombre']; echo ' '; echo $info['apellido']; ?></h3>
                        <p class="text-muted text-center"><b>Administrador/a</b></p>
                      </div>

                      <div class="col-md-9">
                          <div class="row">
                            <div class="col-md-4">
                              <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-users"></i></span>
                                  <div class="info-box-content">
                                  <small class="info-box-text">Usuarios registrados</small>
                                  <h4 class="info-box-number"><?php if(mysqli_num_rows($ucon2)>0) { echo mysqli_num_rows($ucon2); } else { echo '0'; } ?></h4>
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-4">
                              <div class="info-box">
                                <span class="info-box-icon bg-primary"><i class="icono-arg-envio-terrestre"></i></span>
                                  <div class="info-box-content">
                                  <small class="info-box-text">Envios pendientes</small>
                                  <h4 class="info-box-number"><?php if(mysqli_num_rows($econ)>0) { echo mysqli_num_rows($econ); } else { echo '0'; } ?></h4>
                                  </div>
                              </div>
                            </div>
                            <div class="col-md-4 ">
                              <div class="info-box">
                                <span class="info-box-icon bg-dark"><i class="icono-arg-envio-terrestre_1"></i></span>
                                  <div class="info-box-content">
                                  <small class="info-box-text">Envios rápidos pendientes</small>
                                  <h4 class="info-box-number"><?php if(mysqli_num_rows($econ2)>0) { echo mysqli_num_rows($econ2); } else { echo '0'; } ?></h4>
                                  </div>
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-dollar"></i></span>
                                  <div class="info-box-content">
                                  <small class="info-box-text">Dinero depositado de los usuarios</small>
                                  <?php if(mysqli_num_rows($ucon2)>0) { ?>
                                    <?php
                                      while($contar = mysqli_fetch_array($dcon)) {
                                        echo '<h4 class="info-box-number">'.$contar['Total'].'</h4>';
                                      }
                                    ?>
                                  <?php } else { ?>
                                    <h4 class="info-box-number">0</h4>
                                  <?php } ?>
                                  </div>
                              </div>
                            </div>
                          </div>
                      </div>

                    </div>

                  </div>
                </div>

          </div>
        </div>
      </section>
      <!-- Main content -->
      <section class="content container-fluid">
        <div class="card card-primary card-outline">
          <div class="card-body">

            <div class="row">

              <div class="col-md-3">
                <div class="card">
                  <div class="card-header bg-dark text-white" style="background-color:#111111; border-left: 6px solid red;">
                    <h6 class="m-0"><i class="icono-arg-envio-terrestre"></i> Envios pendientes</h6>
                  </div>
                  <div class="card-body">
                    <p class="card-text">
                      <b><?php if(mysqli_num_rows($econ)>0) { echo mysqli_num_rows($econ); } else { echo '0'; } ?></b> Envios pendientes<br>
                    </p>
                    <a href="envios-pendientes.php" class="btn btn-primary btn-block">Acceder</a>
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="card">
                  <div class="card-header bg-dark text-white" style="background-color:#111111; border-left: 6px solid red;">
                    <h6 class="m-0"><i class="icono-arg-factor-pago"></i> Depositos pendientes</h6>
                  </div>
                  <div class="card-body">
                    <p class="card-text">
                      <b><?php if(mysqli_num_rows($dcon2)>0) { echo mysqli_num_rows($dcon2); } else { echo '0'; } ?></b> Depositos pendientes<br>
                    </p>
                    <a href="depositos-pendientes.php" class="btn btn-primary btn-block">Acceder</a>
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="card">
                  <div class="card-header bg-dark text-white" style="background-color:#111111; border-left: 6px solid red;">
                    <h6 class="m-0"><i class="icono-arg-pago"></i> Pagos pendientes</h6>
                  </div>
                  <div class="card-body">
                    <p class="card-text">
                      <b><?php if(mysqli_num_rows($pcon)>0) { echo mysqli_num_rows($pcon); } else { echo '0'; } ?></b> Pagos pendientes<br>
                    </p>
                    <a href="pagos-pendientes.php" class="btn btn-primary btn-block">Acceder</a>
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="card">
                  <div class="card-header bg-dark text-white" style="background-color:#111111; border-left: 6px solid red;">
                    <h6 class="m-0"><i class="icono-arg-tramite"></i> Agregar servicios</h6>
                  </div>
                  <div class="card-body">
                    <p class="card-text">
                      <b><?php if(mysqli_num_rows($scon)>0) { echo mysqli_num_rows($scon); } else { echo '0'; } ?></b> Servicios activos<br>
                    </p>
                    <a href="agregar-servicio.php" class="btn btn-primary btn-block">Acceder</a>
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div class="card">
                  <div class="card-header bg-dark text-white" style="background-color:#111111; border-left: 6px solid red;">
                    <h6 class="m-0"><i class="icono-arg-escritura"></i> Editar usuarios</h6>
                  </div>
                  <div class="card-body">
                    <p class="card-text">
                      <b><?php if(mysqli_num_rows($ucon2)>0) { echo mysqli_num_rows($ucon2); } else { echo '0'; } ?></b> Usuarios<br>
                    </p>
                    <a href="usuarios.php" class="btn btn-primary btn-block">Acceder</a>
                  </div>
                </div>
              </div>

            </div>

          </div>
        </div>

        <?php
          function calculateFileSize($size)
          {
            $sizes = ['B', 'KB', 'MB', 'GB'];
            $count=0;
            if ($size < 1024) {
              return $size . " " . $sizes[$count];
            } else{
              while ($size>1024){
              $size=round($size/1024,2);
              $count++;
            }
              return $size . " " . $sizes[$count];
            }
          }

          $rnom = $_SERVER['DOCUMENT_ROOT'];
        ?>

        <div class="card card-primary card-outline">
          <div class="card-body">
            <form method="post">
                <div class="form-row">
                  <div class="form-group col-md-5 mb-3">
                    <label for="">Subida Limite</label>
                    <input type="text" name="subidal" class="form-control" value="<?php echo $lconfig['subida']; ?>" placeholder="Default: <?php echo calculateFileSize($lconfig['subida']); ?>">
                  </div>
                  <div class="form-group col-md-5 mb-3">
                    <label for="">Foto por defecto</label>
                    <input type="text" name="foto" class="form-control" value="<?php echo $lconfig['foto']; ?>" placeholder="Default: <?php echo $lconfig['foto']; ?>">
                  </div>
                  <div class="form-group col-md-5 mb-3">
                    <label for="">Ruta completa</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1"><?php echo $rnom; ?></span>
                        </div>
                        <input type="text" name="root" class="form-control" value="<?php echo $lconfig['ruta_root']; ?>" placeholder="Default: <?php echo $lconfig['ruta_root']; ?>" aria-describedby="basic-addon1">
                      </div>
                  </div>

                  <div class="form-group col-md-5 mb-3">
                    <label for="">Ruta simple</label>
                    <input type="text" name="file" class="form-control" value="<?php echo $lconfig['ruta_file']; ?>" placeholder="Default: <?php echo $lconfig['ruta_file']; ?>">
                  </div>
                </div>

              <button type="submit" name="cambiar" class="btn btn-primary btn-block mt-3 mb-5">Actualizar</button>
              <?php
                if(isset($_POST['cambiar'])) {
                  $subida = $_POST['subidal'];
                  $rroot = $_POST['root'];
                  $rfile = $_POST['file'];
                  $fotoa = $_POST['foto'];

                  if( empty($subida) || empty($rroot) || empty($rfile) || empty($fotoa) ) {
                    echo '<div class="fixed-bottom">
                        <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                          Faltan campos por completar
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                      </div>';
                  } else {
                    $conexion->query("UPDATE configs SET subida='$subida', ruta_root='$rroot', ruta_file='$rfile', foto='$fotoa'");
                    echo '<div class="fixed-bottom">
                      <div class="alert alert-success alert-dismissible fade show float-right" role="alert">
                        Cambios actualizados
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    </div>';
                    header("Refresh: 1; URL=inicio.php");
                  }

                }
              ?>
            </form>

            <p class="text-center mt-3 mb-5"><span class="text-muted">Limite de subida:</span> <?php echo $lconfig['subida']; echo ' = '; echo calculateFileSize($lconfig['subida']); ?></p>
          </div>
        </div>
      </section>
      <!-- /.content -->
      <!-- /.content -->
    </div>



  </div>
  </body>



  <?php include('../php/footer.php'); ?>

</html>
<?php
  ob_end_flush();
?>
