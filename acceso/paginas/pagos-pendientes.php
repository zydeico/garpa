<!-- === DATOS DE LA PAGINA === -->
<?php
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 0) {
    header("Location: ../index.php");
  }

  $pcon = $conexion->query("SELECT * FROM pagar");

  $titulo = "Pagos Pendientes | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="icono-arg-pago"></i> Pagos pendientes</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-primary card-outline">
                <div class="card-body">
                  <table class="table table-bordered">
                      <tbody>
                        <tr class="bg-dark text-white">
                          <th>Correo</th>
                          <th>Dinero para pagar (+ comisión)</th>
                          <th>Detalles</th>
                          <th>Editar</th>
                        </tr>
                        <?php if(mysqli_num_rows($pcon)>0) { ?>
                          <?php while($pinfo = mysqli_fetch_array($pcon)) { ?>
                            <tr>
                              <td><?php echo $pinfo['correo']; ?></td>
                              <td><?php echo $pinfo['deposito']; echo ' + '; echo $pinfo['tarifa']; ?></td>
                              <td><?php echo $pinfo['detalles']; ?></td>
                              <td>
                                  <a href="../acciones.php?pago=<?php echo $pinfo['iden']; ?>" class="btn btn-danger"><i class="icono-arg-reciclaje-basura"></i></a>
                                  <a href="../acciones.php?pago=<?php echo $pinfo['iden']; ?>&aceptar" class="btn btn-success"><i class="icono-arg-chequeado"></i></a>
                              </td>
                            </tr>
                          <?php } ?>
                        <?php } else { ?>
                          <h2 class="text-center">No hay pagos pendientes</h2>
                        <?php } ?>
                    </tbody>
                  </table>

                </div>
              </div>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
