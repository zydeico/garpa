<!-- === DATOS DE LA PAGINA === -->
<?php
  ob_start();
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 1) {
    header("Location: inicio-admin.php");
  }

  $titulo = "Pagar factura | Garpa Fácil";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="icono-arg-pago"></i> Pagar Factura</h1>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
          <div class="card card-primary card-outline">
            <div class="card-body">
              <form method="post">
                <div class="form-group">
                  <label for="exampleInputPassword1">Servicios a pagar</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" name="servicio" placeholder="Ingrese los servicios que desea pagar.">
                  <small id="" class="form-text text-muted">Los servicios deben estar en el listado del inicio, separados por comas.</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Dinero para abonar</label>
                  <input type="text" class="form-control" id="exampleInputPassword1" name="deposito" placeholder="Ingrese monto [Deposito: <?php echo $info['deposito']; ?>$]">
                  <small id="" class="form-text text-muted">Ingrese el total para abonar, no debe sobre pasar de <?php echo $info['deposito']; ?>$ registrado.</small>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Detalles</label>
                  <small id="" class="form-text text-muted">Detalle que servicios va destinado el dinero a pagar, ej: Luz = 300$, Telefono = 200$.</small>
                  <textarea class="form-control" id="exampleFormControlTextarea1" name="detalles" rows="3"></textarea>
                </div>
                <p>El pago se acredita en 24 horas o menos, la comisiónes son:</p>
                <ul>
                  <li>1 factura o cedulon - 5$AR cada uno</li>
                  <li>5 facturaciones o cedulones - 2$AR cada uno</li>
                  <li>10 facturaciones o cedulones - 1$AR cada uno</li>
                  <li>20 o mas - 0,50$AR cada uno</li>
                </ul>
                <button type="submit" name="pagar" class="btn btn-primary btn-block btn-lg">Pagar factura</button>

                <?php
                  if(isset($_POST['pagar'])) {
                    $correo = $info['correo'];
                    $servicio = $_POST['servicio'];
                    $deposito = $_POST['deposito'];
                    $detalles = $_POST['detalles'];
                    if(is_numeric($deposito)) {
                      if( $deposito != '' || $detalles != '' ) {
                        if( $deposito <= $info['deposito'] ) {
                          date_default_timezone_set('America/Argentina/Buenos_Aires');

                          $iden = rand(0, 10000);

                          $fecha = date("h:i")." ".date("j/n/Y");

                          $conteo = explode(",", $servicio);
                          $ctotal = count($conteo);

                          if($ctotal == 20 || $ctotal >= 21) {
                            $tarifa = "0,50$";
                          } else if($ctotal == 10 || $ctotal == 11 || $ctotal == 12|| $ctotal == 13 || $ctotal == 14 || $ctotal == 15 || $ctotal == 16 || $ctotal == 17  || $ctotal == 18 || $ctotal == 19) {
                            $tarifa = "1$";
                          } else if($ctotal == 5 || $ctotal == 6 || $ctotal == 7 || $ctotal == 8 || $ctotal == 9) {
                            $tarifa = "2$";
                          } else if($ctotal == 0 || $ctotal == 1 || $ctotal == 2 || $ctotal == 3 || $ctotal == 4 ) {
                            $tarifa = "5$";
                          }

                          $conexion->query("INSERT INTO pagar (iden, correo, servicio, deposito, tarifa, detalles, fecha, estado) VALUES ('$iden', '$correo', '$servicio', '$deposito', '$tarifa', '$detalles', '$fecha', '0')");
                          echo '<div class="fixed-bottom">
                            <div class="alert alert-success alert-dismissible fade show float-right" role="alert">
                              Datos enviados.
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                          </div>';
                          header("Refresh: 1; URL=pagar-factura.php");
                        } else {
                          echo '<div class="fixed-bottom">
                            <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                              Deposito insuficinete
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                          </div>';
                        }
                      } else {
                        echo '<div class="fixed-bottom">
                          <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                            Faltan campos sin completar
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                        </div>';
                      }
                    } else {
                      echo '<div class="fixed-bottom">
                        <div class="alert alert-danger alert-dismissible fade show float-right" role="alert">
                          Datos no validos
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                      </div>';
                    }
                  }
                ?>
              </form>
            </div>
          </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>

  <?php include('../php/footer.php'); ?>

</html>
<?php
  ob_end_flush();
?>
