<!-- === DATOS DE LA PAGINA === -->
<?php
  session_start();
  include("../../php/conexion.php");

  if(!isset($_SESSION['user'])) {
    header("Location: ../index.php");
  }

  if($info['rango'] == 1) {
    header("Location: inicio-admin.php");
  }

  $econ = $conexion->query("SELECT * FROM envio WHERE correo='".$_SESSION['user']."'");
  $dcon = $conexion->query("SELECT * FROM pagar WHERE correo='".$_SESSION['user']."' AND estado='0'");

  $servicioscon = $conexion->query("SELECT * FROM servicios WHERE estado='1' ORDER BY id DESC");

  $titulo = "Inicio | Earth Is Water";
  $active_inicio = "active";
?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="fa fa-dashboard"></i> Inicio</h1>
              </div>
          </div>
      </section>

        <!-- Main content -->
        <section class="content container-fluid">
          <div class="row">
            <div class="col-md-12">
                  <div class="card card-primary card-outline">
                    <div class="card-body box-profile">

                      <div class="row">

                        <div class="col-md-3">
                          <div class="text-center">
                              <a href="<?php if(strpos($info['foto'], 'https://') !== false) { echo $lconfig['foto']; } else { echo '../../'; echo $lconfig['ruta_file']; echo $info['foto']; } ?>" data-fancybox="images" data-caption="<?php echo $info['nombre']; echo ' '; echo $info['apellido']; ?>">
                                <img class="profile-user-img img-fluid img-circle" src="<?php if(strpos($info['foto'], 'https://') !== false) { echo $lconfig['foto']; } else { echo '../../'; echo $lconfig['ruta_file']; echo $info['foto']; } ?>" alt="User profile picture">
                              </a>
                          </div>
                          <h3 class="profile-username text-center"><?php echo $info['nombre']; echo ' '; echo $info['apellido']; ?></h3>
                          <p class="text-muted text-center"><b>Usuario/a</b></p>
                        </div>

                        <div class="col-md-9">
                            <div class="row">
                              <div class="col-md-4 mt-5">
                                <div class="info-box">
                                  <span class="info-box-icon bg-success"><i class="fa fa-dollar"></i></span>
                                    <div class="info-box-content">
                                    <small class="info-box-text">Dinero depositado</small>
                                    <h4 class="info-box-number">$<?php echo $info['deposito']; ?></h4>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-4 mt-5">
                                <div class="info-box">
                                  <span class="info-box-icon bg-danger"><i class="fa fa-money"></i></span>
                                    <div class="info-box-content">
                                    <small class="info-box-text">Pendiente de pagos</small>
                                    <h4 class="info-box-number"><?php echo mysqli_num_rows($dcon); ?></h4>
                                    </div>
                                </div>
                              </div>
                              <div class="col-md-4 mt-5">
                                <div class="info-box">
                                  <span class="info-box-icon bg-primary"><i class="fa fa-envelope-o"></i></span>
                                    <div class="info-box-content">
                                    <small class="info-box-text">Pendientes de envio</small>
                                    <h4 class="info-box-number"><?php echo mysqli_num_rows($econ); ?></h4>
                                    </div>
                                </div>
                              </div>
                            </div>
                        </div>

                      </div>

                    </div>
                  </div>

            </div>
          </div>

          <div class="row">

            <div class="col-md-3">
              <div class="card">
                <div class="card-header bg-dark text-white" style="background-color:#111111; border-left: 6px solid red;">
                  <h6 class="m-0"><i class="icono-arg-factor-pago"></i> Agregar deposito</h6>
                </div>
                <div class="card-body">
                  <p class="card-text">
                    Aquí podras agregar dinero a tu cuenta.
                  </p>
                  <a href="agregar-dinero.php" class="btn btn-primary btn-block">Acceder</a>
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="card">
                <div class="card-header bg-dark text-white" style="background-color:#111111; border-left: 6px solid red;">
                  <h6 class="m-0"><i class="icono-arg-envio-terrestre"></i> Agregar envio</h6>
                </div>
                <div class="card-body">
                  <p class="card-text">
                    Aquí podras gestionar el envio de facturas a tu domicilio.
                  </p>
                  <a href="enviar-factura.php" class="btn btn-primary btn-block">Acceder</a>
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="card">
                <div class="card-header bg-dark text-white" style="background-color:#111111; border-left: 6px solid red;">
                  <h6 class="m-0"><i class="fa fa-exclamation"></i> Reclamos</h6>
                </div>
                <div class="card-body">
                  <p class="card-text">
                    ¿No llego su factura a domicilio? ¿No se proceso su deposito?
                  </p>
                  <a href="reclamos.php" class="btn btn-primary btn-block">Acceder</a>
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="card">
                <div class="card-header bg-dark text-white" style="background-color:#111111; border-left: 6px solid red;">
                  <h6 class="m-0"><i class="fa fa-exclamation"></i> Pago de impuestos</h6>
                </div>
                <div class="card-body">
                  <p class="card-text">
                    ¿Desea abonar algun servicio? gestionelo aqui.
                  </p>
                  <a href="pagar-factura.php" class="btn btn-primary btn-block">Acceder</a>
                </div>
              </div>
            </div>


          </div>

          <div class="card card-primary card-outline">
            <div class="card-body">

              <div class="row">
                <div class="col-md-12">
                  <h2>Servicios habilitados:</h2>
                  <hr>
                  <div class="form-group">
                    <input class="form-control" id="yobusco" type="text" placeholder="Buscar">
                  </div>
                </div>
              </div>
              <?php if(mysqli_num_rows($servicioscon)>0) { ?>
                <div class="row">
                  <?php while($sinfo = mysqli_fetch_array($servicioscon)) { ?>
                      <div class="col-md-3 col-xs-12 col-sm-12 buscar">
                        <div class="card">
                          <div class="card-header bg-primary text-white">
                            <center><i class="icono-arg-<?php echo $sinfo['icono']; ?> fa-5x"></i></center>
                          </div>
                          <div class="card-body">
                            <h5><b><?php echo $sinfo['nombre']; ?></b></h5>
                            <p class="card-text"><?php echo utf8_encode($sinfo['descripcion']); ?></p>
                            <div class="dropdown">
                              <a class="btn btn-primary btn-block dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Opciones
                              </a>
                              <div class="dropdown-menu" style="margin:0; padding:0; width:100%;" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item bg-primary" data-caption="<?php echo $sinfo['nombre']; echo ' - '; echo utf8_encode($sinfo['descripcion']); ?>" data-fancybox data-type="iframe" data-src="<?php echo $sinfo['enlace']; ?>" href="javascript:;">
                                Ver factura
                                </a>
                                <a href="#" class="dropdown-item bg-success">Pagar</a>
                                <a href="#" class="dropdown-item bg-dark">Enviar factura a domicilio</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  <?php } ?>
                </div>
              <?php } else { ?>
                <h3 class="text-center">No hay servicios habilitados</h3>
              <?php } ?>

            </div>
          </div>

      </section>
      <!-- /.content -->
    </div>


  </div>
  </body>

  <?php include('../php/footer.php'); ?>
  <script type="text/javascript">

		$(document).ready(function () {
					(function ($) {
							$('#yobusco').keyup(function () {
									var rex = new RegExp($(this).val(), 'i');
									$('.buscar').hide();
									$('.buscar').filter(function () {
											return rex.test($(this).text());
									}).show();
							})
					}(jQuery));
			});
	</script>

</html>
