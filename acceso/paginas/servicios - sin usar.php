<!-- === DATOS DE LA PAGINA === -->
<?php
$titulo = "Servicios | Garpa Fácil";
 ?>
<!DOCTYPE html>
<html lang="es" dir="index.php">
  <head>
    <meta charset="utf-8">
    <?php include('../php/head.php'); ?>
  </head>
  <body class="hold-transition sidebar-mini sidebar-collapse">
  <div class="wrapper">
  <header>
    <?php include('../php/nav.php'); ?>
  </header>

  <div class="content-wrapper" style="min-height: 605px;">
      <!-- Content Header (Page header) -->
      <section class="content-header mt-5">
          <div class="mb-2">
              <div class="card card-body">
                <h1><i class="icono-arg-envio-terrestre"></i> Servicios (vista administrador)</h1>
                <br>
                <a href="agregar-servicios.php" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar servicios</a>
              </div>
          </div>
      </section>

      <!-- Main content -->
      <section class="content container-fluid">
        <div class="card card-primary card-outline">
          <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                  <tr class="bg-dark text-white">
                  <th style="width: 10px">#</th>
                  <th>Servicio</th>
                  <th>URL</th>
                  <th>Editar</th>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>Claro</td>
                  <td>www.claro.com.ar</td>
                  <td><a href="#" class="btn btn-primary"><i class="icono-arg-escritura"></i></a>
                      <a href="#" class="btn btn-danger"><i class="icono-arg-reciclaje-basura"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>Movistar</td>
                  <td>www.movistar.com.ar</td>
                  <td><a href="#" class="btn btn-primary"><i class="icono-arg-escritura"></i></a>
                      <a href="#" class="btn btn-danger"><i class="icono-arg-reciclaje-basura"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>EPEC</td>
                  <td>www.epec.com.ar</td>
                  <td><a href="#" class="btn btn-primary"><i class="icono-arg-escritura"></i></a>
                      <a href="#" class="btn btn-danger"><i class="icono-arg-reciclaje-basura"></i></a>
                  </td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>Telecom</td>
                  <td>www.telecom.com.ar</td>
                  <td><a href="#" class="btn btn-primary"><i class="icono-arg-escritura"></i></a>
                      <a href="#" class="btn btn-danger"><i class="icono-arg-reciclaje-basura"></i></a>
                  </td>
                </tr>
              </tbody></table>
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>



  </div>
  </body>
  <?php include('../php/footer.php'); ?>

</html>
